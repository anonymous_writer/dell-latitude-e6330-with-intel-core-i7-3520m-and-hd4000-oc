/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20160422-64(RM)
 * Copyright (c) 2000 - 2016 Intel Corporation
 * 
 * Disassembling to non-symbolic legacy ASL operators
 *
 * Disassembly of iASL6z3COM.aml, Tue Apr 10 16:35:42 2018
 *
 * Original Table Header:
 *     Signature        "SSDT"
 *     Length           0x00000129 (297)
 *     Revision         0x01
 *     Checksum         0x37
 *     OEM ID           "HD-4000"
 *     OEM Table ID     "anonymou"
 *     OEM Revision     0x00003000 (12288)
 *     Compiler ID      "INTL"
 *     Compiler Version 0x20160422 (538313762)
 */
DefinitionBlock ("", "SSDT", 1, "HD-4000", "anonymou", 0x00003000)
{
    External (_SB_.PCI0, DeviceObj)    // (from opcode)
    External (_SB_.PCI0.IGPU._ADR, UnknownObj)    // (from opcode)

    Scope (\_SB.PCI0)
    {
        Name (GFX0._STA, Zero)  // _STA: Status
        Device (IGPU)
        {
            Name (_ADR, 0x00020000)  // _ADR: Address
            Method (_INI, 0, NotSerialized)  // _INI: Initialize
            {
                Store (Zero, \_SB.PCI0.IGPU._ADR)
            }

            Method (_DSM, 4, NotSerialized)  // _DSM: Device-Specific Method
            {
                If (LEqual (Arg2, Zero))
                {
                    Return (Buffer (One)
                    {
                         0x03                                           
                    })
                }

                Return (Package (0x06)
                {
                    "AAPL,ig-platform-id", 
                    Buffer (0x04)
                    {
                         0x03, 0x00, 0x66, 0x01                         
                    }, 

                    "device-id", 
                    Buffer (0x04)
                    {
                         0x66, 0x01, 0x00, 0x00                         
                    }, 

                    "hda-gfx", 
                    Buffer (0x0A)
                    {
                        "onboard-1"
                    }
                })
            }
        }
    }

    Store ("ssdt-HD4000-www.hackintosh-forum.de-from-anonymous-writer", Debug)
}

